export const mathRound = (number, decimals) => {
  var multiplier;
  multiplier = Math.pow(10, decimals);
  return Math.round(number * multiplier) / multiplier;
};

export const numberRound = (number, decimals) => {
  if (decimals == null) {
    decimals = 0;
  }
  return Math.round(number, decimals);
};

export const format_rupees = (number) => {
  var afterPoint, lastThree, otherNumbers, x;
  x = number.toString();
  afterPoint = "";
  if (x.indexOf(".") > 0) {
    afterPoint = x.substring(x.indexOf("."), x.length);
  }
  x = Math.floor(x);
  x = x.toString();
  lastThree = x.substring(x.length - 3);
  otherNumbers = x.substring(0, x.length - 3);
  if (otherNumbers !== "") {
    lastThree = "," + lastThree;
  }
  return otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
};