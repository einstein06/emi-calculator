import React from 'react';
import { format_rupees, numberRound } from "../helpers/utils";

const AmortizationRow = ({ data }) => {
  return (
    <tr>
      <td>{data.month}</td>
      <td>{format_rupees(numberRound(data.principal))}</td>
      <td>{format_rupees(numberRound(data.interest))}</td>
      <td>{format_rupees(numberRound(data.principal + data.interest))}</td>
      <td>{format_rupees(numberRound(data.balance))}</td>
      <td>{format_rupees(numberRound(data.loan_paid, 2))}%</td>
    </tr>
  );
};

export default AmortizationRow;
