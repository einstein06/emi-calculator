import React from 'react';
import { format_rupees, numberRound } from "../helpers/utils";

const Summary = (props) => {
  return (
    <div>
      <div className="row">
        <div className="col-sm-12">
          <div className="sect">
            <h4 className="badge">Monthly Payment</h4>
            <h3>{format_rupees(numberRound(props.summary.monthly_emi))}</h3>
          </div>
          <div className="sect">
            <h4 className="badge">Total Interest Payable</h4>
            <h3>{format_rupees(numberRound(props.summary.total_interest))}</h3>
          </div>
          <div className="sect">
            <h4 className="badge">Total Payment (Principal + Interest)</h4>
            <h3>{format_rupees(numberRound(props.summary.total_payment))}</h3>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Summary;
