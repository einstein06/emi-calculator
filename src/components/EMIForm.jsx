import React from 'react';
import { useRecoilState } from "recoil";
import { emiConfigAtom } from "../atoms";

const EMIForm = () => {
  const [emiConfig, setEmiConfig] = useRecoilState(emiConfigAtom);

  const handleChange = (e) => {
    const { name, valueAsNumber } = e.currentTarget;
    setEmiConfig((prev) => ({ ...prev, [name]: valueAsNumber }));
  };

  return (
    <div>
      <h1>Home Loan</h1>
      <div data-testid="emi-form" className="form">
        <form className="form-horizontal">
          <div className="form-group">
            <label className="control-label col-md-3" htmlFor="name">
              Amount (&#8377;)
            </label>
            <div className="col-md-4">
              <input
                type="number"
                className="form-control"
                onChange={handleChange}
                name="amount"
                value={emiConfig.amount}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-md-3" htmlFor="interest">
              Interest Rate (%)
            </label>
            <div className="col-md-4">
              <input
                type="number"
                className="form-control"
                onChange={handleChange}
                name="interest"
                value={emiConfig.interest}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-md-3" htmlFor="tenure">
              Tenure
            </label>
            <div className="col-md-3">
              <input
                type="number"
                className="form-control"
                onChange={handleChange}
                name="tenure"
                value={emiConfig.tenure}
              />
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default EMIForm;
