import React from 'react';
import { useRecoilValue } from "recoil";
import { emiCalsAtom } from "../atoms";
import Amortization from './Amortization';
import EMIForm from './EMIForm';
import Summary from './Summary';

const EMICalculator = () => {
  const emiCals = useRecoilValue(emiCalsAtom);
  return (
    <div className="container">
      <h3 id="home" style={{ fontSize: "6rem" }}>EMI Calculator</h3>
      <div className="row">
        <div className="bg-primary col-md-6 col-sm-12" style={{ height: 300 }}>
          <EMIForm />
        </div>
        <div className="bg-warning col-md-6 col-sm-12" style={{ height: 300 }}>
          <Summary summary={emiCals.summary()} />
        </div>
      </div>
      <div className="row">
        <Amortization amortization_schedule={emiCals.amortization_schedule()} />
      </div>
    </div>
  );
};

export default EMICalculator;
