import React from 'react';
import { RecoilRoot } from "recoil";
import EMICalculator from "./components/EMICalculator";
import "./App.css";
import "./styles/bootstrap.css";
import "./styles/main.css";

function App() {
  return (
    <RecoilRoot>
      <div className="App">
        <EMICalculator />
      </div>
    </RecoilRoot>
  );
}

export default App;
