class EMI {
  constructor(P, I, L) {
    this.P = P;
    this.J = I / 12 / 100;
    this.N = L;
  }
  summary() {
    var ret;
    ret = {};
    ret.monthly_emi = this.monthly_emi();
    ret.total_payment = ret.monthly_emi * this.N;
    ret.total_interest = ret.total_payment - this.P;
    return ret;
  }

  amortization_schedule() {
    var P, a, monthly_emi, results, ret;
    a = 0;
    P = this.P;
    monthly_emi = this.monthly_emi();
    results = [];
    while (a < this.N) {
      ret = {};
      ret.month = a + 1;
      ret.interest = Math.round(P * this.J, 4);
      ret.principal = Math.round(monthly_emi - ret.interest, 4);
      ret.balance = Math.round(P - ret.principal, 4);
      ret.loan_paid = (100 * (this.P - ret.balance)) / this.P;
      P = ret.balance;
      a++;
      results.push(ret);
    }
    return results;
  }

  monthly_emi() {
    var ret;
    ret = this.P * (this.J / (1 - Math.pow(1 + this.J, -this.N)));
    return Math.round(ret, 4);
  }
}

export default EMI;
