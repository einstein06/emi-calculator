import { render, screen } from '@testing-library/react';
import App from '../App';

test('renders emi form', () => {
  render(<App />);
  const linkElement = screen.getByText(/EMI Calculator/i);
  expect(linkElement).toBeInTheDocument();
});
