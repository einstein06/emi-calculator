import EMI from '../EMI';
import { format_rupees, numberRound } from '../helpers/utils';

test('renders emi form', () => {
  const emi = new EMI(10000, 12, 1);
  const summary = emi.summary();
  const monthlyEmi = format_rupees(numberRound(summary.monthly_emi));
  const totalInterest = format_rupees(numberRound(summary.total_interest));
  const totalPayment = format_rupees(numberRound(summary.monthly_emi));
  expect(monthlyEmi).toEqual("10,100");
  expect(totalInterest).toEqual("100");
  expect(totalPayment).toEqual("10,100");
});
