import { atom, selector } from 'recoil';
import EMI from './EMI';

export const emiConfigAtom = atom({
  key: "emiConfig",
  default: {
    amount: 1500000,
    interest: 12.75,
    tenure: 12
  }
});

export const emiCalsAtom = selector({
  key: "emiCals",
  get: ({ get }) => {
    const { amount, interest, tenure } = get(emiConfigAtom);
    return new EMI(amount, interest, tenure);
  }
});